# Description:

## get_bitcoin_subset.py
  get_bitcoin_subset.py is designed to create an edge list of the first n transactions.

  The resulting edge list will have the following columns ordered from left to right:
    - Transaction input user
    - Transaction output user
    - Value added to transaction by input user
    - Value received from transaction by output user
    - The epoch time of the transaction.


  Depending on what we end up analyzing, we may need to find some way to integrate
  the transaction value into the edges (Follow up on slack conversation)

  Dependencies are pretty standard, and you shouldnt have any problems running this.


  directory structure:

  This depends on having the following directory structure:

  - bitcoin_networks
      - code
          - get_bitcoin_subset (I run it from this directory)
      - data -> the raw text files that we downloaded
          - addresses.txt
          - blockhash.txt
          - ...
          - txout.txt
      - small_data
          - This directory is where the extracted data subsets will be stored

      - edge_lists
          - this is where the final edge list will be created.

  # Usage:
  First, Make sure that you have the above directory structure
  From the code directory, you can either extract a new subset of the data:

  ``` python3 number_of_transactions ../edge_lists/name_of_edge_list.txt ```

  for example:

  ``` python3 get_bitcoin_subset.py 10000 ../edge_lists/edge_list_10k.txt ```

  Alternatively, you can choose to load files which were previously extracted.

  ```python3 get_bitcoin_subset.py ../small_data/transaction_in_subset ../small_data/transaction_output_subset ../edge_lists/name_of_edge_list.txt```

  for example:

  ```python3 get_bitcoin_subset.py ../small_data/tx_in_1m.txt ../small_data/tx_out_1m.txt ../edge_lists/edge_list_1m.txt```

## contract_tx_files.py
  This script replaces the addrID with the contracted ID from contraction.txt.
  It generates two new files with the replaced IDs: /data/txin_c.txt and /data/txout_x.txt
  It also creates a copy of tx.txt called tx_c.txt
  Finally it sorts the three new files by txID ascending.

  This script will take quite a while to run.  It should not depend on having a large amount of RAM but may run slower if there is not a large amout available.
  The sort operations used 50GB of RAM on my machine and 100% of the CPU and Disk I/O and took ~10 minutes per file.
  You will need 20GB of storage free to run the script.

  This uses the same data directory structure as get_bitcoin_subset.py
  The only dependency is having the gnu sort utility available on the command line. 

## create_edge_list.py
  You MUST run contract_tx_files.py before running this script.
  This script takes a single CL parameter ('n') and writes edge lists for the first n transactions to `/data/edges.txt`.
  Creating a file for the first 10^6 nodes is now trivial, creating a list of 3 million edges in aobut 20 seconds.
