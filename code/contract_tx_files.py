# This script replaces the addrID txin.txt, and txout.txt twith the contracted ID and writes to txin_c.txt and txout_c.txt.
# This was we only need to process the contraction once per machine.
# It also sorts the files by txID to improve sampling algorithms.
#
# Author - Matt Pennington 2017

from os import system

# Build a dictionary of addrID -> contracted ID
contraction_file = open( '../data/contraction.txt' )
addrID_to_contID = {}
line = contraction_file.readline()
while line:
  line = line.split( '\t' )
  if len(line) == 2:
    addrID_to_contID[ int( line[0] ) ] = int( line[1] )
  line = contraction_file.readline()
contraction_file.close()

# Replaces the third column with the contracted ID if it exists.
def replaceAddrIds( in_file, out_file, id_map ):
  line = in_file.readline()
  while line:
    line = line.split('\t')
    if len( line ) >= 4 and int( line[ 2 ] ) in addrID_to_contID:
      line[ 2 ] = str( addrID_to_contID[ int( line[ 2 ] ) ] )
    x = out_file.write( '\t'.join( line ) ) # Assign output to a variable to prevent printing the result to stdout
    line = in_file.readline()
  out_file.close()

# Create new files to hold the contracted files
replaceAddrIds( open( '../data/txin.txt' ), open( '../data/txin_c.txt', 'w' ), addrID_to_contID )
replaceAddrIds( open( '../data/txout.txt' ), open( '../data/txout_c.txt', 'w' ), addrID_to_contID )

# Sort the files, leaving original data as is
system( 'sort txin_c.txt -n -o txin_c.txt' )
system( 'sort txout_c.txt -n -o txout_c.txt' )
system( 'sort tx.txt -n -o tx_c.txt' )
