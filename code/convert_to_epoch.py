import datetime
import json


def convert_to_epoch(date_data):
    date, time = date_data.split("T")
    time = time.strip('.000Z')
    year, month, day = date.split('-')
    hour, minute, second = time.split(':')
    if hour == '': hour = 0
    if minute == '': minute = 0
    if second == '' : second = 0
    epoch = datetime.datetime(int(year), int(month), int(day), int(hour), int(minute), int(second)).strftime('%s')
    return epoch



if __name__ == "__main__":
    with open('../ether_data/price_time.txt', 'r') as f:
        price_time = json.load(f)
    f.close()

    data = price_time['data']

    epoch_data = []
    counter = 0
    for pair in data:
        counter += 1
        if counter == 18:
            print(counter)
        epoch = convert_to_epoch(pair['time'])
        epoch_data.append(
            {
                'time' : epoch,
                'value' : pair['usd']
            }
        )

    results = {
        'data' : epoch_data
    }

    with open('../ether_data/epoch_time.json', 'w') as f:
        json.dump(results, f)
