# Creates a new file btc_edges.txt in the following tab-separated format
# fromContractedId toContractedId edgeWeight txID
#
# txID should be in strict ascending order in all files or this will fail.
# edgeWeight is proportional to the inputs and outputs for each edge

import sys

tx_file = open( '../data/tx_c.txt' )
in_file = open( '../data/txin_c.txt' )
out_file = open( '../data/txout_c.txt' )
edge_file = open( '../data/edges.txt', 'w' )
TAB = '\t'

def create_edge_file( n ):
  tx = tx_file.readline()
  in_line = in_file.readline()
  out_line = out_file.readline()
  i = 0

  #Loop for each transaction while there are still inputs and outputs to read
  while tx and in_line and out_line and i < n:
    txID = int( tx.split(TAB)[0] )
    
    # Fetch the inputs for this transaction
    inputs = []
    txin = in_line.strip().split(TAB)
    while in_line and int( txin[ 0 ] ) <= txID:
      if int( txin[ 0 ] ) == txID:
        inputs.append(txin)
      in_line = in_file.readline()
      txin = in_line.strip().split(TAB)

    # Fetch the outputs for this transaction
    outputs = []
    txout = out_line.strip().split(TAB)
    while int( txout[ 0 ] ) <= txID:
      if int( txout[ 0 ] ) <= txID:
        outputs.append(txout)
      out_line = out_file.readline()
      txout = out_line.strip().split(TAB)

    # Total weight of all outputs
    output_sum = sum( [ int(x[3]) for x in outputs ] )
    
    # Write an edge for each input->output combination, weighting edges proportional to the given input and output
    for txin in inputs:
      for txout in outputs:
        if output_sum > 0:
          weight = int(txout[3]) / output_sum
          value = int( int( txin[3] ) * weight )
          edge = TAB.join( [ txin[2], txout[2], str(value), str(txID) ] ) + '\n'
          edge_file.write( edge )
        else:
          # Should we be considering transations with 0 value transferred? Is there some error here?
          # The check for output_sum > 0 was added because there were some divide by 0 errors
          pass

    i += 1
    tx = tx_file.readline()

if __name__ == "__main__":
  if( len(sys.argv) == 2 ):
    create_edge_file( int( sys.argv[1] ) )
  else:
    print('USAGE: python create_edge_file.py n')
    print('n is the number of transactions to use to create the edge list')
