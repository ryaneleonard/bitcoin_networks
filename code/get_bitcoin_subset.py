import pandas as pd
import numpy as np
import csv
import time
import sys
import os
import pickle
from collections import defaultdict

def get_transaction_data(input_file, output_file, num_transactions):
    ## This function runs in constant time because it must read each line of the input data file.
    start_time = time.time()
    tx_in_list = []
    counter = 0
    with open(input_file, 'r') as f:
        reader = csv.reader(f, delimiter = '\t')
        for row in reader:
            counter += 1
            # if np.mod(counter, 1000000) == 0:
            #     print(counter)
            # Assuming transactions start at 0 and increase incrementally,
            # Create a list of all rows with transaction id's under num_transactions
            if int(row[0]) in range(num_transactions):
                tx_in_list.append(row)

        f.close()
    print("tx_in read runtime = {}".format(time.time() - start_time))
    with open(output_file, 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        writer.writerows(tx_in_list)
        f.close()


def extract_data(number_of_transactions):
    # Output file name generator
    number_of_transactions = int(number_of_transactions)
    tmp = str(number_of_transactions)
    if 999 < number_of_transactions < 1000000:
        suffix = str(tmp)[:-3] + 'k'
    elif 999999 < number_of_transactions < 1000000000:
        suffix = str(tmp)[:-6] + 'm'
    else:
        sys.exit("Please enter a value between 1000 and 1,000,000,000")

    if not os.path.isdir('../small_data'):
        print("There has been an error. Please make sure your directory structure is correct.")
        sys.exit("Please create the directory 'small_data' in your top level bitcoin_networks directory")

    if not os.path.isdir('../data/'):
        sys.exit("Cant find data directory")

    big_tx_in = '../data/txin.txt'
    small_tx_in = '../small_data/tx_in_' + suffix + '.txt'

    big_tx_out = '../data/txout.txt'
    # This is the output file of transaction outputs
    small_tx_out = '../small_data/tx_out_' + suffix + '.txt'


    # Get subset of all data
    print("Parsing transaction inputs. This took 328 seconds on my computer")
    get_transaction_data(big_tx_in, small_tx_in, number_of_transactions)
    print("Parsing Transaction outputs. This took 363 seconds on my computer")
    get_transaction_data(big_tx_out, small_tx_out, number_of_transactions)

    in_df, out_df = create_dataframes(small_tx_in, small_tx_out)
    return small_tx_in, small_tx_out


def create_dataframes(tx_input_file, tx_output_file):
    # Read smaller transaction files into dataframes
    in_df = pd.read_csv(tx_input_file, delimiter='\t')
    in_df.columns = ['tx_id', 'input_id', 'sending_address', 'value']

    out_df = pd.read_csv(tx_output_file, delimiter='\t')
    out_df.columns = ['tx_id', 'output_id', 'receiving_address', 'value']

    ## Get rid of bitcoin spawning transactions
    # First identify transactions to remove
    input_ids = set(in_df.tx_id)
    output_ids = set(out_df.tx_id)
    intersection = input_ids.intersection(output_ids)
    union = input_ids.union(output_ids)
    outliers = union - intersection
    print("There are {} transaction id's which do not occur in both files".format(len(outliers)))
    print("This discrepancy is because miners create bitcoin")

    # Remove transactoins
    print("In before: {}".format(in_df.shape[0]))
    print("Out before: {}".format(out_df.shape[0]))
    in_df = in_df[in_df['tx_id'].isin(intersection)]
    out_df = out_df[out_df['tx_id'].isin(intersection)]
    print("In after: {}".format(in_df.shape[0]))
    print("Out after: {}".format(out_df.shape[0]))
    return in_df, out_df


def get_tx_timestamps(number_of_transactions):
    # tx.txt is sorted by transaction id. Just take rows 0 : num_transactions
    counter = 0
    number_of_transactions = 1000000
    # print("Finding block to transaction relation")
    block_transaction_list = []
    ## tx.txt is a sorted list of transaction ids. Dont search more than we need to or it'll take forever.
    with open('../data/tx.txt', 'r') as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            if counter < number_of_transactions:
                block_transaction_list.append(row)
            else:
                break
            counter += 1
            # if np.mod(counter, 10000) == 0:
            #     print(counter)
        f.close()

    # From the transactions/timestamp list, create a dictionary to efficiently add to dataframe
    tx_time = {}
    for row in block_transaction_list:
        tx_time[row[0]] = row[-1]
    return tx_time


def get_user_lookup_table(in_df, out_df):
    ## Create a dictionary in order to convert addresses to unique user ids.
    start_time = time.time()
    # in_addresses = list(in_df.sending_address.values)
    # out_addresses = list(out_df.receiving_address.values)
    # addresses = list(set(in_addresses + out_addresses))
    # addresses = [str(address) for address in addresses]
#     print(addresses)
    # del in_addresses
    # del out_addresses
    user_lookup_table = {}
    # tx_ids_str = [str(txid) for txid in addresses]
    counter = 0
    with open('../data/contraction.txt', 'r') as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            counter += 1
            if np.mod(counter, 1000000):
                print(counter)
            user_lookup_table[row[0]] = row[1]
        f.close()
    print("User lookup table creation took: {} seconds".format(time.time() - start_time))
    # with open('user_lookup_table.pkl', 'w') as f:
    #     pickle.dump(user_lookup_table, f)
    #     f.close()
    return user_lookup_table


def create_edge_list(in_df, out_df, user_lookup_table):
    tx_ids = list(set(in_df.tx_id))
    edge_list = []
    for value in tx_ids:
        # get the data for the current transaction
        in_tx = in_df.loc[in_df['tx_id'] == value]
        out_tx = out_df.loc[out_df['tx_id'] == value]

        # find the users involved in the current transaction.
        in_tx['user'] = in_tx.apply(lambda row: user_lookup_table[str(row.sending_address)], axis=1)
        out_tx['user'] = out_tx.apply(lambda row: user_lookup_table[str(row.receiving_address)], axis=1)

        for in_row in in_tx.itertuples():
            for out_row in out_tx.itertuples():
                edge_list.append([in_row.user, out_row.user, in_row.value, out_row.value, in_row.time])
    return edge_list


def main(in_df, out_df, edge_list_filepath):
    # a dictionary that gets the timestamps associated with a transaction
    print("Collecting Timestamps")
    tx_time = get_tx_timestamps(in_df.shape[0] + 5000) # Get slightly more transaction records than necessary to make sure that all relevant values are present
    # Add a column for the timestamp based off of the transaction id in each row.
    print("Integrating Timestamps")
    in_df['time'] = in_df.apply(lambda row: tx_time[str(row.tx_id)], axis=1)
    out_df['time'] = out_df.apply(lambda row: tx_time[str(row.tx_id)], axis=1)

    print("Converting Addresses to User ID's")
    user_lookup_table = get_user_lookup_table(in_df, out_df)
    print("Creating Edge List")
    edge_list = create_edge_list(in_df, out_df, user_lookup_table)

    with open(edge_list_filepath, 'w') as f:
        writer = csv.writer(f, delimiter='\t')
        writer.writerows(edge_list)



if __name__ == "__main__":

    # User can either input the relative paths to the data files or create new datafiles by entering a number of transactions to extract.
    if(len(sys.argv) > 3):
        in_transactions = sys.argv[1]
        out_transactions = sys.argv[2]
        edge_list_filepath = sys.argv[3]
    else:
        in_transactions, out_transactions = extract_data(sys.argv[1])
        edge_list_filepath = sys.argv[2]
    in_df, out_df = create_dataframes(in_transactions, out_transactions)
    print("Successfully created input dataframes")
    main(in_df, out_df, edge_list_filepath)
    print("Done")
