# This script initializes a mongodb from the ethereum data.
# It expects that a single file of edges is available in /data/eth_data/ethereum_edge_list.csv
#
# Before running you must have a mongod client running on the default port.
# Typically this is started on Ubuntu by running:
# sudo service mongodb start
#
# Author - Matt Pennington 2017
from bson.objectid import ObjectId
from math import inf as infinity
import pymongo

# construct a list of account dicts in the bg while we loop over the edges


ethereum = pymongo.MongoClient().blockchain.ethereum
ethereum_adj = pymongo.MongoClient().blockchain.ethereum_adj

# def addEdgeToAccount( edge, nodeId ):
#   node = ethereum_adj.find_one( { 'nodeId': nodeId } )
#   if node:
#     node[ 'edges' ].append( edge[ '_id' ] )
#     ethereum_adj.update_one( { 'nodeId': nodeId }, { '$set': node } )
#   else:
#     ethereum_adj.insert_one( { 'nodeId': nodeId, 'edges': [ edge[ '_id' ] ] } )

def main():
  # create list of acct documents that can be loaded into the db after edges
  # are finished
  accts = []
  
  with open( 'acct_metadata_all.csv' ) as acct_data:
    for acct in acct_data:
      acct = acct.strip().split(' ')
      accts.append({
        'address': acct[0],
        'minTimestamp': acct[1],
        'contract' : acct[2],
        'inEdges': [],
        'outEdges': []
      })
      
  import pdb; pdb.set_trace()
  #write edge data
  with open( 'final_etherum_data/ethereum_edge_list.csv' ) as source:
    for line in source:

      line = line.strip().split( ' ' )

      document = {
        'fromId': line[ 0 ],
        'toId': line[ 1 ], 
        'value': float(line[ 2 ]),
        'timestamp': line[ 3 ]
      }

      eid = ethereum.insert_one( document ).inserted_id

      # index into the account list and append the object id
      accts[int(line[0])]['outEdges'].append(eid)
      accts[int(line[1])]['inEdges'].append(eid)

  for node in accts:
    maxTimestamp = -infinity
    edge_ids = node['inEdges'].extend(node['outEdges'])
    for edge in ethereum.find({'_id': {"$in": edge_ids}}):
      if edge[ 'timestamp' ] > maxTimestamp:
        maxTimestamp = edge[ 'timestamp' ]
    node[ 'maxTimestamp' ] = maxTimestamp 

    ethereum_adj.insertMany( accts, {ordered: True} )

  client.blockchain.ethereum.create_index([('timestamp', pymongo.ASCENDING)])
  client.blockchain.ethereum_adj.create_index([('minTimestamp', pymongo.ASCENDING)])

if __name__ == "__main__":
  main()