# Visualization server/client
Dependencies: mongodb, pymongo, flask, flask-restful, flask-cors

Code is modified from the following original sources:
https://bl.ocks.org/mbostock/ad70335eeef6d167bc36fd3c04378048
https://bl.ocks.org/pkerpedjiev/f2e6ebb2532dae603de13f0606563f5b

## Initial setup

Run `python3 /code/init_eth_data.py` to read the edgelist from /data/eth_data/ethereum_edge_list.csv into a mongodb collection.  
This takes several hours!
You can clear a large amount of RAM after the script runs by restarting mongo. E.g. on ubuntu run `sudo service mongodb restart`.

## To run the application

1. `cd` to the /code/js-d3-viz directory
2. Start the client: `python3 -m http.server 80`
3. Start the server: `python3 server.py`
4. Open http://localhost in a browser.  The first network you see is the miserables data.  Hit click me to fetch the first few transactions from the ethereum data.
