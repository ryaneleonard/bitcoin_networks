import networkit as nkt
from queue import Queue
from random import sample, random, choice, randrange


class Sampler(object):

    @staticmethod
    def streamingBFS(n, eCursor, wSize=500):

        '''
        n - desired number of nodes
        eCursor - a mongodb cursor for the edge collection (to iterate over all edges)
        NOTE: for temporal data, the collection should be sorted by time
        wSize - the size of the window/reservoir to keep
        '''
        vs = {}
        es = []
        ew = []
        nw = set()
        t = wSize
        q = Queue()
        total = eCursor.count()

        # populate the window with edges
        while len(ew) < wSize:
            edge_data = eCursor.next()
            edge = (
                edge_data['fromId'],
                edge_data['toId'],
                edge_data['value'],
                edge_data['timestamp']
            )
            ew.append(edge)
            nw |= {edge[0], edge[1]}

        u = sample(nw, 1)[0]

        # assuming that the cursor will not reiterate over the previously retrieved
        # edges, we can use the following for loop
        # otherwise, we'll need to create a while loop and test the return value of
        # collection.next()
        for edge_data in eCursor:
            # if i == (total - 5):
                # import pdb; pdb.set_trace()

            edge = (
                edge_data['fromId'],
                edge_data['toId'],
                edge_data['value'],
                edge_data['timestamp']
            )
            
            if u not in vs:
                vs[u] = {'in': [], 'out': []}

            inc_edges = [ei for ei in ew if u in ei[:2]]

            if len(inc_edges) > 0:
                e = choice(inc_edges)
                es.append(e)
                ew.remove(e)
                v = e[0 if e.index(u) else 1]

                if v not in vs:
                    vs[v] = {'in': [], 'out': []}

                if e.index(u) == 1:
                    vs[u]['in'].append((v, e[3]))
                    vs[v]['out'].append((u, e[3]))
                else:
                    vs[u]['out'].append((v, e[3]))
                    vs[v]['in'].append((u, e[3]))

                q.put(v)

            else:
                if q.empty():
                    u = sample(nw, 1)[0]
                else:
                    u = q.get()

            # move the window
            if len(ew) == wSize:
                del ew[0]
            ew.append(edge)

            if len(vs) > n:
                # import pdb; pdb.set_trace()
                while len(vs) > n:
                    # select random edge and remove
                    er = choice(es)
                    es.remove(er)
                    vs[er[0]]['out'].remove((er[1], er[3]))
                    vs[er[1]]['in'].remove((er[0], er[3]))

                    # check if either incident vertex is now removed from the graph
                    # and remove
                    if len(vs[er[0]]['out']) == 0 and len(vs[er[0]]['in']) == 0:
                        del vs[er[0]]
                    if len(vs[er[1]]['out']) == 0 and len(vs[er[1]]['in']) == 0:
                        del vs[er[1]]

                break
            t += 1
        return vs, es

    @staticmethod
    def streamingNoGraphPIES(ssize, eCursor):
        # vs[u] = [edge indices]
        vs = {}
        es = []
        e_idx = 0
        t = 1
        m = 0
        nid = 0

        for edge in eCursor:
            et = (edge['fromId'], edge['toId'], edge['value'], edge['timestamp'])
            u, v = et[0], et[1]

            if len(vs) < ssize:
                if u not in vs:
                    vs[u] = [nid, []]
                    nid += 1

                if v not in vs:
                    vs[v] = [nid, []]
                    nid += 1

                es.append(et)
                vs[u][1].append(e_idx)
                vs[v][1].append(e_idx)

                e_idx += 1
                m = len(es)
            else:
                if random() <= m/t:
                    # import pdb; pdb.set_trace()
                    i,k = sample(list(vs), 2)

                    if u not in vs:
                        for ir in vs[i][1]:
                            es[ir] = None
                        del vs[i]
                        vs[u] = [nid, []]
                        nid += 1

                    if v not in vs:
                        for kr in vs[k][1]:
                            es[kr] = None
                        del vs[k]
                        vs[v] = [nid, []]
                        nid += 1

                if u in vs and v in vs:
                    es.append(et)
                    vs[u][1].append(e_idx)
                    vs[v][1].append(e_idx)
                    e_idx += 1
                # if t % 1000 == 0:
                #     print('t = %d' % t)
            t += 1
            if t % 100000 == 0:
                print('t = %d' % t)
        # import pdb; pdb.set_trace()
        vl = [i[0] for i in sorted(list(vs.items()), key=lambda x: x[1][0])]
        return vl, [e for e in es if e]

    @staticmethod
    def streamingSetPIES(n, eCursor):
        # vs[u] = [edge indices]
        vs = set()
        es = set()
        t = 1
        m = 0

        for edge in eCursor:
            et = (edge['fromId'], edge['toId'], edge['value'], edge['timestamp'])
            u, v = et[0], et[1]

            if len(vs) < n:

                vs |= {u, v}

                es.add(et)
                m = len(es)
            else:
                if random() <= m/t:
                    
                    i,k = sample(vs, 2)

                    if u not in vs:
                        es -= {e for e in es if i in e[:2]}
                        vs.remove(i)

                    # import pdb; pdb.set_trace()
                    if v not in vs:
                        es -= {e for e in es if k in e[:2]}
                        vs.remove(k)

                    vs |= {u, v}

                if u in vs and v in vs:
                    es.add(et)
            t += 1
            if t % 100000 == 0:
                print('t = %d' % t)
        return vs, es

    @staticmethod
    def edgeSample(numEdges, cursor):
        vertices = []
        end = cursor.count()
        eids = sample(range(end), numEdges)
        edges = [cursor[i] for i in eids]

    
    # @staticmethod
    def streamingPIES(n, eCursor):
        print('n = %d' % n)

        g = nkt.Graph(0, True, True)

        t = 1
        m = 0

        vMap = {}
        nid = 0

        for edge in eCursor:

            et = (edge['fromId'], edge['toId'], edge['value'], edge['timestamp'])
            u, v = et[0], et[1]

            if g.numberOfNodes() < n:

                if u not in vMap:
                    vMap[u] = nid
                    g.addNode()
                    nid += 1
                    
                if v not in vMap:
                    vMap[v] = nid
                    g.addNode()
                    nid += 1

                g.addEdge(vMap[u], vMap[v], et[2])
                
                m = g.numberOfEdges()
            else:

                if random() <= (m/t):

                    i,k = sample(list(vMap), 2)

                    if u not in vMap:
                        g.removeNode(vMap[i])
                        del vMap[i]
                        g.addNode()
                        vMap[u] = nid
                        nid += 1

                    if v not in vMap:
                        g.removeNode(vMap[k])
                        del vMap[k]
                        g.addNode()
                        vMap[v] = nid
                        nid += 1

                if (u in vMap) and (v in vMap):
                    g.addEdge(vMap[u], vMap[v], et[2])
                if t % 10000 == 0:
                    print('t = %d' % t)
            t += 1
            if t % 100000 == 0:
                print('t = %d' % t)
        return g.nodes(), g.edges()
    #     

# from graph_sampler import *
# eth = pymongo.MongoClient().blockchain.ethereum_edges
# sort_obj = [['timestamp', pymongo.ASCENDING],['ord', pymongo.ASCENDING]]
# query_obj = {'timestamp': {'$gte': 1498867200, '$lt': 1504224000}}
# cursor = eth.find(query_obj).sort(sort_obj)
# v, e = Sampler.streamingNoGraphPIES(2000, cursor)
# cursor.rewind()
# vs, es = Sampler.streamingSetPIES(2000, cursor)