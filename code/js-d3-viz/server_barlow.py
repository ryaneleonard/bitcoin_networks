# Blockchain network server
# This is an interface to create graph data from mongodb for the client in index.html
# It serves the API on http://localhost:5000
#
# Author - Matt Pennington 2017

from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS
import pymongo
from queue import Queue
from random import sample, random, choice

# Define collections
ethereum = pymongo.MongoClient().blockchain.ethereum_edges
# TODO: add additional collections when available (e.g. BTC)

# Define app parameters
app = Flask(__name__)
api = Api(app)
CORS(app)

print('Initializing address map ...')
with open( '../../acct_metadata_all.tsv' ) as file:
  addresses = [ line.split('\t')[0] for line in file ]
print('done.')


def sampleEdges( edges, strategies={} ):
  # TODO: implement me!!!
  return edges

def sampleGraph( nodes, links, strategies={} ):
  # TODO: implement me!!!

  if 'value' in strategies:
    nodes = [ node for node in nodes if node[ 'value' ] >= int( strategies[ 'value' ] ) ]
  if 'indegree' in strategies:
    nodes = [ node for node in nodes if node[ 'indegree' ] >= int( strategies[ 'indegree' ] ) ]
  if 'outdegree' in strategies:
    nodes = [ node for node in nodes if node[ 'outdegree' ] >= int( strategies[ 'outdegree' ] ) ]
  if 'degree' in strategies:
    nodes = [ node for node in nodes if ( node[ 'outdegree' ] + node[ 'indegree' ] ) >= int( strategies[ 'degree' ] ) ]

  nodelist = [ node['id'] for node in nodes ]
  links = [ link for link in links if link['source'] in nodelist and link['target'] in nodelist ]

  if 'post-sample-edges' in strategies and len( links ) > 10000:
    links = sample( links, 10000 )

  return { 'nodes': nodes, 'links': links }

def getStatistics( graph, statistics={} ):
  # TODO: implement me!!!
  return {}

# Serve the ethereum collection
class Ethereum_Network(Resource):
  def get(self, from_time, to_time):
    import pdb;pdb.set_trace()
    # Query the db by timestamp and sample the edges.  
    links = ethereum.find( { 'timestamp': { '$lt': int(to_time), '$gt': int(from_time) } }, { '_id':0 } )
    print ('Number of query results: ', links.count() )
    links = sampleEdges( list( links ), {} )

    # Build the graph in the same format as miserables.json
    # { nodes: [ { id, value, indegree, outdegree }, ... ], links: [ { source, target, value }, ... ] }
    links = [ { 'source': edge['fromId'], 'target': edge['toId'], 'value': edge['value'], 'timestamp': edge['timestamp'] } for edge in links ]
    
    if len( links ) > 10:
      print( links[:10])
    else:
      print( links )

    nodes = { nodeid: { 'id': nodeid, 'address': addresses[ int(nodeid) ], 'value': 0, 'indegree': 0, 'outdegree': 0 } 
              for nodeid in set( [ link['source'] for link in links ] + [ link['target'] for link in links ] ) }
    for link in links:
      nodes[ link[ 'source' ] ][ 'value' ] -= link[ 'value' ]
      nodes[ link[ 'target' ] ][ 'value' ] += link[ 'value' ]
      nodes[ link[ 'source' ] ][ 'outdegree' ] += 1
      nodes[ link[ 'target' ] ][ 'indegree' ] += 1
    nodes = list( nodes.values() )

    # Sample the graph
    graphStrategies = {}
    if 'nodevalue' in request.args:
      graphStrategies[ 'value' ] = request.args.get('nodevalue')
    if 'indegree' in request.args:
      graphStrategies[ 'indegree' ] = request.args.get('indegree')
    if 'outdegree' in request.args:
      graphStrategies[ 'outdegree' ] = request.args.get('outdegree')
    if 'degree' in request.args:
      graphStrategies[ 'degree' ] = request.args.get('degree')
    if 'post-sample-edges' in request.args:
      graphStrategies[ 'post-sample-edges' ] = request.args.get('post-sample-edges')
    graph = sampleGraph( nodes , links, graphStrategies )

    # Perform network analysis
    statistics = getStatistics( graph, [] )

    # Serve it up nice and hot!
    return { 'graph': graph, 'statistics': statistics }

# Routes
api.add_resource( Ethereum_Network, '/eth/<string:from_time>/<string:to_time>' )
# TODO: add additional routes as needed, e.g. for additional collections

if __name__ == '__main__':
  app.run()