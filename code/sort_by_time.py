import csv
import sys
my_list = []
val = 10 ** 12
with open('/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/ethereum_edge_list.csv', 'r') as f:
    reader = csv.reader(f, delimiter=' ')
    for row in reader:
        my_list.append(row)
    f.close()

my_list.sort(key=lambda x: float(x[-1]))

with open('/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/time_sorted_ethereum_edge_list.csv', 'w') as f:
    writer = csv.writer(f, delimiter=' ')
    writer.writerows(my_list)
