from graph_tool.all import  *
import json
import numpy as np
import csv
import datetime
import time
import configuration_model_analysis



big_data = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/time_sorted_ethereum_edge_list.csv'

# Small datafiles to be created.
june_1_data = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/june_1_data/june_1_edge_list.csv'
july_1_data = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/june_1_data/july_1_edge_list.csv'

# start_time = 1498867200


def convert_to_epoch(date_data):
    date, time = date_data.split("T")
    time = time.strip('.000Z')
    year, month, day = date.split('-')
    hour, minute, second = time.split(':')
    if hour == '': hour = 0
    if minute == '': minute = 0
    if second == '' : second = 0
    epoch = datetime.datetime(int(year), int(month), int(day), int(hour), int(minute), int(second)).strftime('%s')
    return epoch


def get_date_edge_list(big_data, small_data, start_time, num_hours):
    end_time = start_time + (3600 * num_hours)
    edge_list = []
    with open(big_data, 'r') as f:
        reader = csv.reader(f, delimiter=' ')
        for row in reader:
            if float(row[-1]) > float(end_time):
                break
            if float(row[-1]) > float(start_time):
                edge_list.append(row)
    f.close()
    with open(small_data, 'w') as f:
        writer = csv.writer(f, delimiter=' ')
        writer.writerows(edge_list)
    f.close()


def get_data_buckets(datafile, start_time, num_hours):
    edge_list = []
    with open(datafile, 'r') as f:
        reader = csv.reader(f, delimiter=' ')
        for row in reader:
            edge_list.append(row)
    f.close()

    #iterate through the edge list, and create hourly sublists.
    bucket_list = []
    bucket = []
    while len(edge_list) > 1:
        end_time = start_time + (3600 * num_hours)
        edge = edge_list.pop() # Popping goes in reverse order.
        # Get the 1 hour boundary value.
        bucket_boundary = 3600 * (len(bucket_list) + 1)
        if int(edge[-1]) < end_time - bucket_boundary:
            bucket_list.append(bucket)
            bucket = []
        else:
            bucket.append(edge)
    # Append the final bucket
    bucket_list.append(bucket)
    return bucket_list


def get_n_largest_components(G, num_components):
    v, hist = graph_tool.topology.label_components(G)
    n_component_labels = hist.argsort()[-num_components:]

    graph_list = []
    for value in n_component_labels:
        graph_list.append(graph_tool.GraphView(G, vfilt=(v.a == value)))
    return graph_list


def gather_metrics(bucket_list, num_hours, window_size, start_time):
    data_dump = {}

    for i in range(num_hours - window_size + 1):
        iter_start = time.time()
        cur_list = []
        for j in range(window_size):
            cur_list += bucket_list[i + j]

        G = graph_tool.Graph()
        v_props = G.add_edge_list(cur_list, hashed=True, string_vals=True)
        G.vertex_properties['vertex_id'] = v_props
        tot_vertices = G.num_vertices()
        tot_in_degrees = G.get_in_degrees(G.get_vertices())
        tot_out_degrees = G.get_out_degrees(G.get_vertices())

        centrality_prop_map = graph_tool.centrality.eigenvector(G)
        G.vertex_properties['centrality'] = centrality_prop_map[1]
        centrality_scores = [centrality_prop_map[1][j] for j in range(G.num_vertices())]


#         print(tot_in_degrees == tot_out_degrees)
        tot_clustering_coefficient = graph_tool.clustering.global_clustering(G)  # Value, stddev
        # The total value traded within this window.
        tot_value = [float(val[2]) for val in cur_list]
        tot_value = sum(tot_value)

        # subgraphs = get_n_largest_components(G, 3)
        # subgraphs_data_list = []
        # for graph in subgraphs:
        #     sub_vertices = graph.num_vertices()
        #     sub_in_degrees = graph.get_in_degrees(G.get_vertices())
        #     sub_out_degrees = graph.get_out_degrees(G.get_vertices())
        #     #             print(sub_in_degrees == sub_out_degrees)
        #     sub_clustering_coefficient = graph_tool.clustering.global_clustering(graph)
        #
        #     subgraph_data = {
        #         "num_vertices": sub_vertices,
        #         "in_degrees": sub_in_degrees.tolist(),
        #         "out_degrees": sub_out_degrees.tolist(),
        #         "clustering_coefficient": sub_clustering_coefficient
        #     }
        #     subgraphs_data_list.append(subgraph_data)

        subgraphs = get_n_largest_components(G, 1)
        subgraphs_data_list = []
        for graph in subgraphs:
            sub_vertices = graph.num_vertices()
            sub_in_degrees = graph.get_in_degrees(G.get_vertices())
            sub_out_degrees = graph.get_out_degrees(G.get_vertices())
            #             print(sub_in_degrees == sub_out_degrees)
            sub_clustering_coefficient = graph_tool.clustering.global_clustering(graph)

            subgraph_data = {
                "num_vertices": sub_vertices,
                "in_degrees": sub_in_degrees.tolist(),
                "out_degrees": sub_out_degrees.tolist(),
                "clustering_coefficient": sub_clustering_coefficient
            }
            subgraphs_data_list.append(subgraph_data)

        # Dump Graph Data
        iter_timestamp = start_time + 3600 * i + 3600 * window_size
        iter_data = {
            "timestamp": iter_timestamp,
            "total_value": tot_value,
            "num_vertices": tot_vertices,
            "in_degrees": tot_in_degrees.tolist(),
            "out_degrees": tot_out_degrees.tolist(),
            "centrality_scores":centrality_scores,
            "clustering_coefficient": tot_clustering_coefficient,
            "subgraph_1": subgraphs_data_list[0]
        }

        # # pass data to configuration model for testing
        # config_iter_data = configuration_model_analysis.main(G, iter_data)
        #
        # # Add configuration model simulation data to results.
        # iter_data["configuration_model"] = config_iter_data

        data_dump['iter_{}'.format(i)] = iter_data
        print("Completed iter {} in {} seconds".format(i, time.time() - iter_start))
    return data_dump


def main(infile, outfile):
    # start_time = 1498867200
    # adsfadfads = 1496296800

    big_data = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/time_sorted_ethereum_edge_list.csv'
    july_1_data = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/june_1_data/july_1_edge_list.csv'

    # Get all transaction data within a given window
    bucket_list = get_data_buckets(infile, start_time=start_time, num_hours=24)

    # iterate through the data in hour increments.
    num_hours = 24
    window_size = 4

    data_dump = gather_metrics(bucket_list, num_hours, window_size, start_time)

    with open(outfile, 'w') as f:
        json.dump(data_dump, f)
        f.close()

    # return iter_data_list


if __name__ == "__main__":
    infile = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/june_1_data/july_1_edge_list.csv'
    outfile = 'test_data_1.json'
    iter_data_list = main(infile, outfile)