from graph_tool.all import *
import numpy as np
from collections import defaultdict
import examine_time_series
import collect_time_series


def configuration_model_on_iteration(data, num_repititions):
    # Use the configuration model to create a graph and collect metrics on the generated graph
    eigenvector_centrality_scores = []
    assortativity_values = []
    global_clustering_coefficients = []
    fraction_of_vertices_in_largest_component = []

    in_degrees = data['in_degrees']
    out_degrees = data['out_degrees']
    for i in range(num_repititions):
        deg_data = list(zip(in_degrees, out_degrees))
        g = graph_tool.generation.random_graph(N=len(in_degrees), deg_sampler=lambda x: deg_data.pop())

        # Centrality Scores
        centrality_prop_map = graph_tool.centrality.eigenvector(g)
        centrality_scores = [centrality_prop_map[1][j] for j in range(len(in_degrees))]
        eigenvector_centrality_scores.append(centrality_scores)

        # Assortativity
        assortativity_values.append(graph_tool.correlations.assortativity(g, 'total'))

        # Clustering
        global_clustering_coefficients.append(graph_tool.clustering.global_clustering(g))

        # Fraction of vertices in largest component.
        largest_component = collect_time_series.get_n_largest_components(g, 1)
        largest_component = largest_component[0]
        fraction_of_vertices_in_largest_component.append(float(largest_component.num_vertices()) / float(g.num_vertices()))
    return eigenvector_centrality_scores, assortativity_values, global_clustering_coefficients, fraction_of_vertices_in_largest_component


def centrality_score_analysis(configuration_scores, true_values):
    ## Modified for graph_tool
    # Create a matrix for the differences:
    config_scores = np.array(configuration_scores)
    true_scores = np.array(true_values)
    diff_scores = config_scores - true_scores

    idx = np.where(np.isnan(diff_scores))
    diff_scores[idx[:]] = 0

    mean_diff = np.ones([1, len(true_values)])
    p_25_dff = np.ones([1, len(true_values)])
    p_75_dff = np.ones([1, len(true_values)])
    # Get the mean and percentile scores of the configuration model
    for i in range(diff_scores.shape[1]):
        cur_mean = np.mean(diff_scores[:, i])
        mean_diff[0, i] = np.mean(diff_scores[:, i])
        mean_diff[0, i]
        p_25_dff[0, i] = np.mean(np.percentile(diff_scores[:, i], 25))
        p_75_dff[0, i] = np.mean(np.percentile(diff_scores[:, i], 75))

    return mean_diff.tolist(), p_25_dff.tolist(), p_75_dff.tolist()

    # # From homework 2:
    # mean_list = []
    # p_25_list = []
    # p_75_list = []
    # # median_list = []
    # # plot_data = []
    # for i in range(len(model_diff.keys())):
    #     mean = np.mean(model_diff[i])
    #     #     median = np.percentile(model_data[i], 50)
    #
    #     p_25 = np.percentile(model_diff[i], 25)
    #     p_75 = np.percentile(model_diff[i], 75)
    #     #     plot_data.append([p_25, mean, p_75])
    #     #     median_list.append(median)
    #     mean_list.append(mean)
    #     p_25_list.append(p_25)
    #     p_75_list.append(p_75)
    # idx = [i for i in range(len(mean_list))]
    #
    #
    # # zero = [0 for i in r]
    # fig, ax = plt.pyplot.subplots(1, figsize=(15, 15))
    # ax.plot(idx, mean_list, label='Mean', color='b')
    # # ax.plot(idx, median_list, label = 'Median', color='g')
    # ax.plot(idx, p_75_list, 'r', label='Percentiles')
    # ax.plot(idx, p_25_list, 'r', )
    # ax.plot(idx, [0] * len(idx) , 'k')
    # ax.fill_between(idx, p_75_list, p_25_list, facecolor='yellow', alpha=0.25)
    # ax.legend()
    # ax.set_xlabel("Node Number")
    # ax.set_ylabel("Distance")
    # # ax.set_xticklabels([i[0] for i in index], minor=False)


def main(G, iter_data):
    eigenvector_centrality_scores, assortativity_values, global_clustering_coefficients, fraction_of_vertices_in_largest_component = configuration_model_on_iteration(iter_data, 100)


    # centrality_prop_map = graph_tool.centrality.eigenvector(G)
    # true_scores = [centrality_prop_map[1][j] for j in range(G.num_vertices())]
    mean_diff, p_25_diff, p_75_diff = centrality_score_analysis(eigenvector_centrality_scores, iter_data['centrality_scores'])
    # Create a node ID conversion table
    mean_diff_final = []
    p_25_diff_final = []
    p_75_diff_final = []
    asdf = G.num_vertices()
    sdf = len(mean_diff)
    for i in range(G.num_vertices()):
        vid = G.vertex_properties['vertex_id'][i]
        mean_diff_final.append([vid, mean_diff[0][i]])
        p_25_diff_final.append([vid, p_25_diff[0][i]])
        p_75_diff_final.append([vid, p_75_diff[0][i]])

    config_iter_data = {
        "centrality_mean": mean_diff_final,
        "centrality_p25": p_25_diff_final,
        "centrality_p75": p_75_diff_final,
        "assortativity": np.mean(assortativity_values),
        "clustering_coefficieints": np.mean(global_clustering_coefficients),
        "vertices_ratio": np.mean(fraction_of_vertices_in_largest_component),
    }

    return config_iter_data


