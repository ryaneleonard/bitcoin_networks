import matplotlib.pyplot as plt
import json
import os
import examine_time_series


def get_price_data_between_times(start_time, end_time):
    price_data = []
    with open('/home/ryan/git-repos/bitcoin_networks/ether_data/epoch_time.json', 'r') as f:
        total_price_data = json.load(f)
    f.close()

    total_price_data = total_price_data['data']
    for data in total_price_data:
        if int(data['time']) > end_time:
            break
        if int(data['time']) > start_time:
            price_data.append([int(data['time']), int(data['value'])])
    return price_data


def get_deltas(xvals, yvals):
    new_list = []
    for i in range(1, len(yvals)):
        new_list.append(yvals[i] - yvals[i-1])
    return xvals[1:], new_list


def plot_vertex_relationships(results1, results2, results3, network_timestamps1, network_timestamps2, network_timestamps3, price_timestamps, prices, plots_dir):
    ## Examine relationship between number of vertices
    ratios1 = []
    ratios2 = []
    ratios3 = []
    for i in range(len(results1['num_vertices'])):
        ratios1.append(float(results1['num_subgraph_vertices'][i]) / float(results1['num_vertices'][i]))

    for i in range(len(results2['num_vertices'])):
        ratios2.append(float(results2['num_subgraph_vertices'][i]) / float(results2['num_vertices'][i]))

    for i in range(len(results3['num_vertices'])):
        ratios3.append(float(results3['num_subgraph_vertices'][i]) / float(results3['num_vertices'][i]))
    # ratios.append(float(num_subgraph_vettices[i]) / float(num_vertices[i]))

    f1, axarr = plt.subplots(3, sharex=True, figsize=(10, 10))
    axarr[0].plot(network_timestamps1, results1['num_vertices'], label="Window Size 2")
    axarr[0].plot(network_timestamps2, results2['num_vertices'], label="Window Size 5")
    axarr[0].plot(network_timestamps3, results3['num_vertices'], label="Window Size 10")
    axarr[0].set_title('Number Of Vertices')
    axarr[1].plot(network_timestamps1, results1['num_subgraph_vertices'], label="Window Size 2")
    axarr[1].plot(network_timestamps2, results2['num_subgraph_vertices'], label="Window Size 5")
    axarr[1].plot(network_timestamps3, results3['num_subgraph_vertices'], label="Window Size 10")
    axarr[1].set_title('Largest Component Vertices')
    axarr[2].plot(network_timestamps1, ratios1, label="Window Size 2")
    axarr[2].plot(network_timestamps2, ratios2, label="Window Size 5")
    axarr[2].plot(network_timestamps3, ratios3, label="Window Size 10")
    axarr[2].set_title('Fraction of Vertices in Largest Component')
    axarr[2].set_xlabel('Hours From Start Of Data')
    plt.legend(loc=1)
    plt.savefig(plots_dir + "/vertex_relationship.png")

    ## Examine relationship between number of vertices
    ratios1 = []
    ratios2 = []
    ratios3 = []
    for i in range(len(results1['num_vertices'])):
        ratios1.append(float(results1['num_subgraph_vertices'][i])/float(results1['num_vertices'][i]))

    for i in range(len(results2['num_vertices'])):
        ratios2.append(float(results2['num_subgraph_vertices'][i])/float(results2['num_vertices'][i]))

    for i in range(len(results3['num_vertices'])):
        ratios3.append(float(results3['num_subgraph_vertices'][i])/float(results3['num_vertices'][i]))

    price_delta_timestamps, price_deltas = get_deltas(price_timestamps, prices)
    delta_timestamps1, delta_vertices1 = get_deltas(network_timestamps1, results1['num_vertices'])
    delta_timestamps1, delta_subgraph_vertices1 = get_deltas(network_timestamps1, results1['num_subgraph_vertices'])
    delta_timestamps1, delta_ratios1 = get_deltas(network_timestamps1, ratios1)

    delta_timestamps2, delta_vertices2 = get_deltas(network_timestamps2, results2['num_vertices'])
    delta_timestamps2, delta_subgraph_vertices2 = get_deltas(network_timestamps2, results2['num_subgraph_vertices'])
    delta_timestamps2, delta_ratios2 = get_deltas(network_timestamps2, ratios2)

    delta_timestamps3, delta_vertices3 = get_deltas(network_timestamps3, results3['num_vertices'])
    delta_timestamps3, delta_subgraph_vertices3 = get_deltas(network_timestamps3, results3['num_subgraph_vertices'])
    delta_timestamps3, delta_ratios3 = get_deltas(network_timestamps3, ratios3)

    f2, axarr = plt.subplots(3, sharex=True, figsize=(10, 10))
    axarr[0].plot(delta_timestamps1, delta_vertices1, label="Window Size 2")
    axarr[0].plot(delta_timestamps2, delta_vertices2, label="Window Size 5")
    axarr[0].plot(delta_timestamps3, delta_vertices3, label="Window Size 10")
    axarr[0].set_title('Change in Number of Vertices')

    axarr[1].plot(delta_timestamps1, delta_subgraph_vertices1, label="Window Size 2")
    axarr[1].plot(delta_timestamps2, delta_subgraph_vertices2, label="Window Size 5")
    axarr[1].plot(delta_timestamps3, delta_subgraph_vertices3, label="Window Size 10")
    axarr[1].set_title('Change in Number of Largest Component Vertices')
    axarr[2].plot(delta_timestamps1, delta_ratios1, label="Window Size 2")
    axarr[2].plot(delta_timestamps2, delta_ratios2, label="Window Size 5")
    axarr[2].plot(delta_timestamps3, delta_ratios3, label="Window Size 10")
    axarr[2].set_title('Change in Ratio of Vertices')
    # Plot y = 0 on delta plots
    axarr[0].axhline(0, color='orange', linestyle='--')
    axarr[1].axhline(0, color='orange', linestyle='--')
    axarr[2].axhline(0, color='orange', linestyle='--')
    axarr[2].set_xlabel('Hours From Start Of Data')
    plt.legend(loc=1)

    plt.savefig(plots_dir + "/delta_vertex_relationship.png")


def plot_clustering_coefficient_relationship(results1, results2, results3, network_timestamps1, network_timestamps2, network_timestamps3, price_timestamps, prices, plots_dir):
    ## Examine Clustering Coefficient Relationship
    f1, axarr = plt.subplots(2, sharex=True, figsize=(10, 10))
    axarr[0].plot(network_timestamps1, [val[0] for val in results1['clustering_coefficients']], label="Window Size 2")
    axarr[0].plot(network_timestamps2, [val[0] for val in results2['clustering_coefficients']], label="Window Size 5")
    axarr[0].plot(network_timestamps3, [val[0] for val in results3['clustering_coefficients']], label="Window Size 10")
    axarr[0].set_title('Clustering Coefficient')
    axarr[1].plot(network_timestamps1, [val[0] for val in results1['subgraph_clustering_coefficients']], label="Window Size 2")
    axarr[1].plot(network_timestamps2, [val[0] for val in results2['subgraph_clustering_coefficients']], label="Window Size 5")
    axarr[1].plot(network_timestamps3, [val[0] for val in results3['subgraph_clustering_coefficients']], label="Window Size 10")
    axarr[1].set_title('Subgraph Clustering Coefficient')
    axarr[1].set_xlabel('Hours From Start Of Data')
    plt.legend(loc=1)
    plt.savefig(plots_dir + "/clustering_coefficient_relationship.png")


    ## Examine Clustering Coefficient Relationship
    price_delta_timestamps, price_deltas = get_deltas(price_timestamps, prices)
    delta_timestamps1, delta_clustering_coefficient1 = get_deltas(network_timestamps1, [val[0] for val in results1['clustering_coefficients']])
    delta_timestamps1, delta_delta_subgraph_clustering_coefficient1 = get_deltas(network_timestamps1, [val[0] for val in results1['subgraph_clustering_coefficients']])

    delta_timestamps2, delta_clustering_coefficient2 = get_deltas(network_timestamps2, [val[0] for val in results2['clustering_coefficients']])
    delta_timestamps2, delta_delta_subgraph_clustering_coefficient2 = get_deltas(network_timestamps2, [val[0] for val in results2['subgraph_clustering_coefficients']])

    delta_timestamps3, delta_clustering_coefficient3 = get_deltas(network_timestamps3, [val[0] for val in results3['clustering_coefficients']])
    delta_timestamps3, delta_delta_subgraph_clustering_coefficient3 = get_deltas(network_timestamps3, [val[0] for val in results3['subgraph_clustering_coefficients']])

    f2, axarr = plt.subplots(2, sharex=True, figsize=(10, 10))
    axarr[0].plot(delta_timestamps1, delta_clustering_coefficient1, label="Window Size 2")
    axarr[0].plot(delta_timestamps2, delta_clustering_coefficient2, label="Window Size 5")
    axarr[0].plot(delta_timestamps3, delta_clustering_coefficient3, label="Window Size 10")
    axarr[0].set_title('Clustering Coefficient delta')

    axarr[1].plot(delta_timestamps1, delta_delta_subgraph_clustering_coefficient1, label="Window Size 2")
    axarr[1].plot(delta_timestamps2, delta_delta_subgraph_clustering_coefficient2, label="Window Size 5")
    axarr[1].plot(delta_timestamps3, delta_delta_subgraph_clustering_coefficient3, label="Window Size 10")
    axarr[1].set_title('Subgraph Clustering Coefficient delta')
    axarr[1].set_xlabel('Hours From Start Of Data')
    axarr[0].axhline(0,color='orange', linestyle='--')
    axarr[1].axhline(0,color='orange', linestyle='--')
    plt.legend(loc=1)
    plt.savefig(plots_dir + "/delta_clustering_coefficient_relationship.png")


def plot_degree_relationship(results1, results2, results3, network_timestamps1, network_timestamps2, network_timestamps3, price_timestamps, prices, plots_dir):
    ## Examine In/Out Degree Relationship
    # TODO: Figure out what is going on with the average in degree and average out degree
    f1, axarr = plt.subplots(2, sharex=True, figsize=(10, 10))
    axarr[0].plot(network_timestamps1, results1['average_in_degrees'], label="Window Size 2")
    axarr[0].plot(network_timestamps2, results2['average_in_degrees'], label="Window Size 5")
    axarr[0].plot(network_timestamps3, results3['average_in_degrees'], label="Window Size 10")
    axarr[0].set_title('Average In-Degree')

    axarr[1].plot(network_timestamps1, results1['subgraph_average_in_degrees'], label="Window Size 2")
    axarr[1].plot(network_timestamps2, results2['subgraph_average_in_degrees'], label="Window Size 5")
    axarr[1].plot(network_timestamps3, results3['subgraph_average_in_degrees'], label="Window Size 10")
    axarr[1].set_title('Average Degree of Largest Component')
    axarr[1].set_xlabel('Hours From Start Of Data')
    plt.legend(loc=1)
    plt.savefig(plots_dir + "/degree_relationship.png")

    delta_timestamps1, delta_in_degree1 = get_deltas(network_timestamps1, results1['average_in_degrees'])
    delta_timestamps1, delta_subgraph_in_degree1 = get_deltas(network_timestamps1, results1['subgraph_average_in_degrees'] )

    delta_timestamps2, delta_in_degree2 = get_deltas(network_timestamps2, results2['average_in_degrees'])
    delta_timestamps2, delta_subgraph_in_degree2 = get_deltas(network_timestamps2, results2['subgraph_average_in_degrees'] )

    delta_timestamps3, delta_in_degree3 = get_deltas(network_timestamps3, results3['average_in_degrees'])
    delta_timestamps3, delta_subgraph_in_degree3 = get_deltas(network_timestamps3, results3['subgraph_average_in_degrees'] )

    ## Examine In/Out Degree Relationship
    # TODO: Figure out what is going on with the average in degree and average out degree
    f, axarr = plt.subplots(2, sharex=True, figsize=(10, 10))

    axarr[0].plot(delta_timestamps1, delta_in_degree1, label="Window Size 2")
    axarr[0].plot(delta_timestamps2, delta_in_degree2, label="Window Size 5")
    axarr[0].plot(delta_timestamps3, delta_in_degree3, label="Window Size 10")
    axarr[0].set_title('Delta Mean In-Degree')
    axarr[0].axhline(0, color='orange', linestyle='--')

 
    axarr[1].plot(delta_timestamps1, delta_subgraph_in_degree1, label="Window Size 2")
    axarr[1].plot(delta_timestamps2, delta_subgraph_in_degree2, label="Window Size 5")
    axarr[1].plot(delta_timestamps3, delta_subgraph_in_degree3, label="Window Size 10")
    axarr[1].set_title('Delta Subgraph Mean In-Degree')
    axarr[1].axhline(0, color='orange', linestyle='--')


    axarr[1].set_xlabel('Hours From Start Of Data')
    plt.legend(loc=1)
    plt.savefig(plots_dir + "/delta_degree_relationship.png")


def plot_value_relationship(results1, results2, results3, network_timestamps1, network_timestamps2, network_timestamps3, price_timestamps, prices, plots_dir):
    ## Examine In/Out Degree Relationship
    # TODO: Figure out what is going on with the average in degree and average out degree
    f, axarr = plt.subplots(2, sharex=True, figsize=(10, 10))
    axarr[0].plot(price_timestamps, prices)
    axarr[0].set_title('Ethereum Price')
    axarr[0].set_ylabel("USD")
    axarr[1].plot(network_timestamps, results['total_value'])
    axarr[1].set_title('Value Traded')
    axarr[1].set_ylabel("Ethereum Denomination")
    axarr[1].set_xlabel('Hours From Start Of Data')

    plt.savefig(plots_dir + "/value_relationship.png")



def create_all_figures(results1, results2, results3, plots_dir, start_epoch, end_epoch):
    # TODO: Create Evolving Graph images
    #     - Node color based on just added or just removed
    #     - Edge width based on value transferred
    #     - Node Size based on sum of in-degrees
    # TODO: Figure out how to incorporate edge weights
    # TODO: Figure out most effective way to plot changing histograms.

    # network_timestamps = results['timestamps']
    network_timestamps1 = [(val - start_epoch) / 3600 for val in results1['timestamps']]
    network_timestamps2 = [(val - start_epoch) / 3600 for val in results2['timestamps']]
    network_timestamps3 = [(val - start_epoch) / 3600 for val in results3['timestamps']]

    price_data = get_price_data_between_times(start_epoch, end_epoch)
    # price_timestamps = [data[0] for data in price_data]

    price_timestamps = [(data[0] - start_epoch)/3600 for data in price_data]

    prices = [data[1] for data in price_data]
    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)

    plot_vertex_relationships(results1, results2, results3, network_timestamps1, network_timestamps2, network_timestamps3, price_timestamps, prices, plots_dir)
    plot_clustering_coefficient_relationship(results1, results2, results3, network_timestamps1, network_timestamps2, network_timestamps3, price_timestamps, prices, plots_dir)
    plot_degree_relationship(results1, results2, results3, network_timestamps1, network_timestamps2, network_timestamps3, price_timestamps, prices, plots_dir)
    # plot_value_relationship(results1, results2, results3, network_timestamps1, network_timestamps2, network_timestamps3, price_timestamps, prices, plots_dir)


if __name__ == "__main__":
    results1 = examine_time_series.main('/home/ryan/git-repos/bitcoin_networks/ether_data/network_metrics/edges_from_9_April_2016_buckets_100_window_2.json')
    results2 = examine_time_series.main('/home/ryan/git-repos/bitcoin_networks/ether_data/network_metrics/edges_from_9_April_2016_buckets_100_window_5.json')
    results3 =examine_time_series.main('/home/ryan/git-repos/bitcoin_networks/ether_data/network_metrics/edges_from_9_April_2016_buckets_100_window_10.json')
    plots_dir = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_plots'
    start_epoch = 1460181600
    end_epoch = start_epoch + (3600 * 100)
    create_all_figures(results1, results2, results3, plots_dir, start_epoch, end_epoch)


