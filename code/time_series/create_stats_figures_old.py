import matplotlib.pyplot as plt
import json
import os


def get_price_data_between_times(start_time, end_time):
    price_data = []
    with open('/home/ryan/git-repos/bitcoin_networks/ether_data/epoch_time.json', 'r') as f:
        total_price_data = json.load(f)
    f.close()

    total_price_data = total_price_data['data']
    for data in total_price_data:
        if int(data['time']) > end_time:
            break
        if int(data['time']) > start_time:
            price_data.append([int(data['time']), int(data['value'])])
    return price_data


def get_deltas(xvals, yvals):
    new_list = []
    for i in range(1, len(yvals)):
        new_list.append(yvals[i] - yvals[i-1])
    return xvals[1:], new_list


def plot_vertex_relationships(results, network_timestamps, price_timestamps, prices, plots_dir):
    ## Examine relationship between number of vertices
    ratios = []
    for i in range(len(results['num_vertices'])):
        ratios.append(float(results['num_subgraph_vertices'][i]) / float(results['num_vertices'][i]))
    # ratios.append(float(num_subgraph_vettices[i]) / float(num_vertices[i]))

    f1, axarr = plt.subplots(4, sharex=True, figsize=(10, 10))
    axarr[0].plot(price_timestamps, prices)
    axarr[0].set_title('Ethereum Price')
    axarr[1].plot(network_timestamps, results['num_vertices'])
    axarr[1].set_title('Number Of Vertices')
    axarr[2].plot(network_timestamps, results['num_subgraph_vertices'])
    axarr[2].set_title('Largest Component Vertices')
    axarr[3].plot(network_timestamps, ratios)
    axarr[3].set_title('Fraction of Vertices in Largest Component')
    axarr[3].set_xlabel('Hours From Start Of Data')

    plt.savefig(plots_dir + "/vertex_relationship.png")

    ## Examine relationship between number of vertices
    ratios = []
    for i in range(len(results['num_vertices'])):
        ratios.append(float(results['num_subgraph_vertices'][i])/float(results['num_vertices'][i]))


    price_delta_timestamps, price_deltas = get_deltas(price_timestamps, prices)
    delta_timestamps, delta_vertices = get_deltas(network_timestamps, results['num_vertices'])
    delta_timestamps, delta_subgraph_vertices = get_deltas(network_timestamps, results['num_subgraph_vertices'])
    delta_timestamps, delta_ratios = get_deltas(network_timestamps, ratios)

    f2, axarr = plt.subplots(4, sharex=True, figsize=(10, 10))
    axarr[0].plot(price_delta_timestamps, price_deltas)
    axarr[0].set_title('Ethereum Price Change')
    axarr[1].plot(delta_timestamps, delta_vertices)
    axarr[1].set_title('Change in Number of Vertices')
    axarr[2].plot(delta_timestamps, delta_subgraph_vertices)
    axarr[2].set_title('Change in Number of Largest Component Vertices')
    axarr[3].plot(delta_timestamps, delta_ratios)
    axarr[3].set_title('Change in Ratio of Vertices')
    # Plot y = 0 on delta plots
    axarr[0].axhline(0, color='orange', linestyle='--')
    axarr[1].axhline(0, color='orange', linestyle='--')
    axarr[2].axhline(0, color='orange', linestyle='--')
    axarr[3].axhline(0, color='orange', linestyle='--')
    axarr[3].set_xlabel('Hours From Start Of Data')

    plt.savefig(plots_dir + "/delta_vertex_relationship.png")


def plot_clustering_coefficient_relationship(results, network_timestamps, price_timestamps, prices, plots_dir):
    ## Examine Clustering Coefficient Relationship
    f1, axarr = plt.subplots(3, sharex=True, figsize=(10, 10))
    axarr[0].plot(price_timestamps, prices)
    axarr[0].set_title('Ethereum Price')
    axarr[1].plot(network_timestamps, [val[0] for val in results['clustering_coefficients']])
    axarr[1].set_title('Clustering Coefficient')
    axarr[2].plot(network_timestamps, [val[0] for val in results['subgraph_clustering_coefficients']])
    axarr[2].set_title('Subgraph Clustering Coefficient')
    axarr[2].set_xlabel('Hours From Start Of Data')
    plt.savefig(plots_dir + "/clustering_coefficient_relationship.png")

    ## Examine Clustering Coefficient Relationship
    price_delta_timestamps, price_deltas = get_deltas(price_timestamps, prices)
    delta_timestamps, delta_clustering_coefficient = get_deltas(network_timestamps, [val[0] for val in results['clustering_coefficients']])
    delta_timestamps, delta_delta_subgraph_clustering_coefficient = get_deltas(network_timestamps, [val[0] for val in results['subgraph_clustering_coefficients']])

    f2, axarr = plt.subplots(3, sharex=True, figsize=(10, 10))
    axarr[0].plot(price_delta_timestamps, price_deltas)
    axarr[0].set_title('Ethereum Price delta')
    axarr[1].plot(delta_timestamps, delta_clustering_coefficient)
    axarr[1].set_title('Clustering Coefficient delta')
    axarr[2].plot(delta_timestamps, delta_delta_subgraph_clustering_coefficient)
    axarr[2].set_title('Subgraph Clustering Coefficient delta')
    axarr[2].set_xlabel('Hours From Start Of Data')

    axarr[0].axhline(0,color='orange', linestyle='--')
    axarr[1].axhline(0,color='orange', linestyle='--')
    axarr[2].axhline(0,color='orange', linestyle='--')
    plt.savefig(plots_dir + "/delta_clustering_coefficient_relationship.png")


def plot_degree_relationship(results, network_timestamps, price_timestamps, prices, plots_dir):
    ## Examine In/Out Degree Relationship
    # TODO: Figure out what is going on with the average in degree and average out degree
    f1, axarr = plt.subplots(3, sharex=True, figsize=(10, 10))
    axarr[0].plot(price_timestamps, prices)
    axarr[0].set_title('Ethereum Price USD')
    axarr[1].plot(network_timestamps, results['average_in_degrees'])
    axarr[1].set_title('Average Degree')
    axarr[2].plot(network_timestamps, results['subgraph_average_in_degrees'])
    axarr[2].set_title('Average Degree of Largest Component')
    axarr[2].set_xlabel('Hours From Start Of Data')
    # axarr[1].set_title('Average In Degree')
    # axarr[2].plot(network_timestamps, results['average_out_degrees'])
    # axarr[2].set_title('Average Out Degree')
    # axarr[3].plot(network_timestamps, results['subgraph_average_in_degrees'])
    # axarr[3].set_title('Average Subgraph in Degree')
    # axarr[4].plot(network_timestamps, results['subgraph_average_out_degrees'])
    # axarr[4].set_title('Average Subgraph Out Degree')
    # axarr[4].set_xlabel('Hours From Start Of Data')
    plt.savefig(plots_dir + "/degree_relationship.png")

    price_delta_timestamps, price_deltas = get_deltas(price_timestamps, prices)
    delta_timestamps, delta_in_degree = get_deltas(network_timestamps, results['average_in_degrees'])
    delta_timestamps, delta_subgraph_in_degree = get_deltas(network_timestamps,results['subgraph_average_in_degrees'] )
    delta_timestamps, delta_out_degree = get_deltas(network_timestamps,results['average_out_degrees'])
    delta_timestamps, delta_subgraph_out_degree = get_deltas(network_timestamps, results['subgraph_average_out_degrees'])

    ## Examine In/Out Degree Relationship
    # TODO: Figure out what is going on with the average in degree and average out degree
    f, axarr = plt.subplots(3, sharex=True, figsize=(10, 10))
    eth_price = axarr[0].plot(price_delta_timestamps, price_deltas)
    axarr[0].set_title('Delta Ethereum Price')
    axarr[0].axhline(0, color='orange', linestyle='--')

    axarr[1].plot(delta_timestamps, delta_in_degree)
    axarr[1].set_title('Delta Mean Degree')
    axarr[1].axhline(0, color='orange', linestyle='--')

    # axarr[2].plot(delta_timestamps, delta_out_degree)
    # axarr[2].set_title('Delta Out Degree')
    # axarr[2].axhline(0, color='orange', linestyle='--' )

    axarr[2].plot(delta_timestamps, delta_subgraph_in_degree)
    axarr[2].set_title('Delta Subgraph Mean Degree')
    axarr[2].axhline(0, color='orange', linestyle='--')


    # axarr[4].plot(delta_timestamps, delta_subgraph_out_degree)
    # axarr[4].set_title('Delta Subgraph Out Degree')
    # axarr[4].axhline(0,color='orange', linestyle='--' )

    axarr[2].set_xlabel('Hours From Start Of Data')
    plt.savefig(plots_dir + "/delta_degree_relationship.png")


def plot_value_relationship(results, network_timestamps, price_timestamps, prices, plots_dir):
    ## Examine In/Out Degree Relationship
    # TODO: Figure out what is going on with the average in degree and average out degree
    f, axarr = plt.subplots(2, sharex=True, figsize=(10, 10))
    axarr[0].plot(price_timestamps, prices)
    axarr[0].set_title('Ethereum Price')
    axarr[0].set_ylabel("USD")
    axarr[1].plot(network_timestamps, results['total_value'])
    axarr[1].set_title('Value Traded')
    axarr[1].set_ylabel("Ethereum Denomination")
    axarr[1].set_xlabel('Hours From Start Of Data')

    plt.savefig(plots_dir + "/value_relationship.png")


def plot_configuration_model_stats(results):
    # Plot the configuration model ratio over time


    # Plot the configuration model assortativity value over time


    # Plot the configuration model clustering coefficient over time.
    return None


def create_all_figures(results, plots_dir, start_epoch, end_epoch):
    # TODO: Create Evolving Graph images
    #     - Node color based on just added or just removed
    #     - Edge width based on value transferred
    #     - Node Size based on sum of in-degrees
    # TODO: Figure out how to incorporate edge weights
    # TODO: Figure out most effective way to plot changing histograms.

    # july_1_data = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/june_1_data/july_1_edge_list.csv'
    #
    # collect_time_series.main(infile = july_1_data, outfile='/home/ryan/git-repos/bitcoin_networks/code/test_data_1.json')
    # results = examine_time_series.main('/home/ryan/git-repos/bitcoin_networks/code/test_data_1.json')
    network_timestamps = results['timestamps']
    network_timestamps = [(val - start_epoch) / 3600 for val in results['timestamps']]

    price_data = get_price_data_between_times(start_epoch, end_epoch)
    # price_timestamps = [data[0] for data in price_data]

    price_timestamps = [(data[0] - start_epoch)/3600 for data in price_data]

    prices = [data[1] for data in price_data]
    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)

    plot_vertex_relationships(results, network_timestamps, price_timestamps, prices, plots_dir)
    plot_clustering_coefficient_relationship(results, network_timestamps, price_timestamps, prices, plots_dir)
    plot_degree_relationship(results, network_timestamps, price_timestamps, prices, plots_dir)
    plot_value_relationship(results, network_timestamps, price_timestamps, prices, plots_dir)

