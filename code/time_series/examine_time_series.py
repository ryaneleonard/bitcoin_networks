from graph_tool.all import *
import numpy as np
import json
import matplotlib.pyplot as plt
import datetime


def read_data(datafile):
    with open(datafile, 'r') as f:
        results = json.load(f)
    f.close()
    return results


def get_largest_component(iter_data):
    # As we experiment with different window sizes, it may prove useful to look at the different subgraphs.
    # However, for now, lets just look at the largest.
    largest_graph = iter_data['subgraph_1']
    # subgraph_2 = iter_data['subgraph_2']
    # subgraph_3 = iter_data['subgraph_3']
    # if largest_graph['num_vertices'] < subgraph_2['num_vertices']:
    #     largest_graph = subgraph_2
    # if largest_graph['num_vertices'] < subgraph_3['num_vertices']:
    #     largest_graph = subgraph_3
    return largest_graph


def extract_data(data):
    ## Create an ordered list of data from every iteration
    timestamps = []
    total_value = []
    num_vertices = []
    num_subgraph_vertices = []
    in_degree_list = []
    subgraph_in_degree_list = []
    out_degree_list = []
    subgraph_out_degree_list = []
    clustering_coefficients = []
    subgraph_clustering_coefficients = []

    average_in_degrees = []
    subgraph_average_in_degrees = []
    average_out_degrees = []
    subgraph_average_out_degrees = []

    # Do some extraction stuff
    num_iters = len(data.keys())
    for i in range(num_iters):
        iter_data = data['iter_{}'.format(i)]
        subgraph_data = get_largest_component(iter_data)

        # Timestamp
        timestamps.append(iter_data['timestamp'])

        # Value traded
        total_value.append(iter_data['total_value'])

        # Number of Vertices
        num_vertices.append(iter_data['num_vertices'])
        num_subgraph_vertices.append(subgraph_data['num_vertices'])

        # In Degree Structure
        in_degree_list.append(iter_data['in_degrees'])
        subgraph_in_degree_list.append(subgraph_data['in_degrees'])

        # Out Degree Structure
        out_degree_list.append(iter_data['out_degrees'])
        subgraph_out_degree_list.append(subgraph_data['out_degrees'])

        # Clustering Coefficients
        clustering_coefficients.append(iter_data['clustering_coefficient'])
        subgraph_clustering_coefficients.append(subgraph_data['clustering_coefficient'])

        # Average In/Out Degrees
        average_in_degrees.append(np.mean(iter_data['in_degrees']))
        subgraph_average_in_degrees.append(np.mean(subgraph_data['in_degrees']))
        average_out_degrees.append(np.mean(iter_data['out_degrees']))
        subgraph_average_out_degrees.append(np.mean(subgraph_data['out_degrees']))

    # Store all extracted lists in a dictionary for convenient plotting.
    results = {
        'timestamps': timestamps,
        'total_value': total_value,
        'num_vertices': num_vertices,
        'num_subgraph_vertices': num_subgraph_vertices,
        'in_degree_list': in_degree_list,
        'subgraph_in_degree_list': subgraph_in_degree_list,
        'out_degree_list': out_degree_list,
        'subgraph_out_degree_list': subgraph_out_degree_list,
        'clustering_coefficients': clustering_coefficients,
        'subgraph_clustering_coefficients': subgraph_clustering_coefficients,
        'average_in_degrees': average_in_degrees,
        'subgraph_average_in_degrees': subgraph_average_in_degrees,
        'average_out_degrees': average_out_degrees,
        'subgraph_average_out_degrees': subgraph_average_out_degrees,
    }

    return results


def get_price_data_between_times(start_time, end_time):
    price_data = []
    with open('../ether_data/epoch_time.json', 'r') as f:
        total_price_data = json.load(f)
    f.close()

    total_price_data = total_price_data['data']
    for data in total_price_data:
        if int(data['time']) > end_time:
            break
        if int(data['time']) > start_time:
            price_data.append([int(data['time']), int(data['value'])])
    return price_data


def main(datafile):
    loaded_file = read_data(datafile)
    results = extract_data(loaded_file)

    # Lets try plotting num_vertices vs time

    return results


if __name__ == "__main__":
    datafile = '/home/ryan/git-repos/bitcoin_networks/code/test_data_1.json'
    results = main(datafile)
