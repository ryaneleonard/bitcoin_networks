import csv
import datetime
import os
import json
import collect_time_series
import examine_time_series
import create_stats_figures


data_file = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/time_sorted_ethereum_edge_list.csv'
# june_1_data = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/june_1_data/june_1_edge_list.csv'
# july_1_data = '/home/ryan/git-repos/bitcoin_networks/ether_data/final_etherum_data/june_1_data/july_1_edge_list.csv'

def date_to_epoch(year, month, day):
    return datetime.datetime(int(year), int(month), int(day), 0, 0, 0).strftime('%s')


def get_date_edge_list(data_file, small_data, start_time, num_hours):
    # If we have already run this configuration, reuse the file.
    if os.path.isfile(small_data):
        return None
    end_time = start_time + (3600 * num_hours)
    edge_list = []
    with open(data_file, 'r') as f:
        reader = csv.reader(f, delimiter=' ')
        for row in reader:
            if float(row[-1]) > float(end_time):
                break
            if float(row[-1]) > float(start_time):
                edge_list.append(row)
    f.close()
    with open(small_data, 'w') as f:
        writer = csv.writer(f, delimiter=' ')
        writer.writerows(edge_list)
    f.close()
    return None


def main(start_date, num_hours, window_size):
    # Start_time_format year-month-day, ex: 2017-12-10
    year, month, day = start_date.split('-')
    year = int(year)
    month = int(month)
    day = int(day)

    start_epoch = int(date_to_epoch(year, month, day))
    end_epoch = start_epoch + (3600 * num_hours)

    month_string = datetime.date(1900, month, 1).strftime('%B')
    filename = 'edges_from_{}_{}_{}_buckets_{}_window_{}'.format(str(day), month_string, str(year), num_hours, window_size)
    date_subset_file = '/home/ryan/git-repos/bitcoin_networks/ether_data/edge_list_subsets/' + filename + '.csv'
    # Create an edge list subset to run more efficiently
    get_date_edge_list(data_file, date_subset_file, start_epoch, num_hours)
    date_metrics_file = '/home/ryan/git-repos/bitcoin_networks/ether_data/network_metrics/' + filename + '.json'

    if os.path.isfile(date_metrics_file):
        with open(date_metrics_file, 'r') as f:
            data_dump = json.load(f)
        f.close()
    else:
        # Group the edges between even hour values, i.e. 1:00, 2:00, etc..
        bucket_list = collect_time_series.get_data_buckets(date_subset_file, start_epoch, num_hours)
        # Provided with buckets of edges created between given times, Gather network metrics and store them in a dictionary.
        data_dump = collect_time_series.gather_metrics(bucket_list, num_hours, window_size, start_epoch)
        # Save results
        with open(date_metrics_file, 'w') as f:
            json.dump(data_dump, f)
        f.close()

    # Here, we will filter what metrics we want to view.
    # Right now, the main filter applied returns the largest component from the reported 3 largest components
    results = examine_time_series.extract_data(data_dump)
    plots_dir = '/home/ryan/git-repos/bitcoin_networks/ether_data/plots/' + filename
    create_stats_figures.create_all_figures(results, plots_dir, start_epoch, end_epoch)
    return None



if __name__ == '__main__':
    import time
    start = time.time()
    main('2016-4-9', 100,  2)
    print("Finished Window 2")
    main('2016-4-9', 100,  5)
    print("Finished Window 5")
    main('2016-4-9', 100,  10)
    print("Finished Window 10")
    print("Total Runtime: {} seconds".format(time.time()-start))
