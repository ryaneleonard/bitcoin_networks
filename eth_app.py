from collections import OrderedDict
import csv
import pickle
import time
from web3 import Web3, IPCProvider

# replace with your own path
ipc_endpoint = '/Users/mbarlow/Library/Ethereum/geth.ipc'
web3 = Web3( IPCProvider( ipc_endpoint ))

# first = 46147 Michael: I'll manually get the first blocks up to 100000
first = 4100000
chunk_size = 10000
blocks_per_file = 300000
last = first + ( blocks_per_file )

# list of account tuples: ( hash, timestamp (first seen), contract? (boolean))
accounts = {'none'}
a_file_name = 'account-data.csv'

with open(a_file_name, 'r') as account_data:
    reader = csv.reader( account_data, delimiter=',' )
    for row in reader:
        accounts.add(row[0])

print("{} accounts imported".format(len(accounts)))

# for i in range(first, last, blocks_per_file):

# transactions-(i-chunksize)-i.csv
# t_file_name = 'transactions-{}-{}.csv'.format(i, i+blocks_per_file-1)
t_file_name = 'transactions-{}-{}.csv'.format(first, last-1)

for j in range( first, last, chunk_size ):
    chunk_transactions_string = ""
    chunk_accounts_string = ""

    for block_number in range( j, j + chunk_size ):

        # get data for single block
        block_data = web3.eth.getBlock( block_number, True)

        # set the timestamp
        t_stamp = str(block_data['timestamp'])

        accounts_in_block = {'none'}

        if block_data.transactions:
            t_list = []
            for t in block_data.transactions:
                value = t['value']
                f = t['from'] if t['from'] else 'none'
                tto = t['to'] if t['to'] else 'none'
                t_list.append(",".join([f, tto, str(value), t_stamp]) + "\n")

                accounts_in_block |= {f, tto}

            chunk_transactions_string += "".join(t_list)

        accounts_in_block.remove('none')
        new_accounts = accounts_in_block - accounts

        for account in new_accounts:
            contract = not web3.eth.getCode(account) == "0x"
            chunk_accounts_string += ",".join([account, t_stamp, str(contract)]) + "\n"
        accounts |= new_accounts

        if block_number % 2000 == 0:
            print("at block %d" % block_number)

    # end loop, should have 10000 blocks finished
    
    with open( t_file_name, 'a' ) as t_file:
        t_file.write( chunk_transactions_string )

    with open( a_file_name, 'a' ) as a_file:
        a_file.write( chunk_accounts_string )

    print( "blocks {} to {} processed".format(j, block_number) )



