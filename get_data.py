from blockchain import blockexplorer
import time
import pickle
import os

def get_next_valid_block(current_block):
    next_block = blockexplorer.get_block(current_block.previous_block)
    # Recursively search for valid blocks.
    if next_block.main_chain == False:
        return(get_next_valid_block(next_block))
    return next_block


def create_block_list(starting_block_height, ending_block_height=0):
    # It is better to run this code and store a copy of the data locally than it is
    # to download the dataset again every time you want to run an experiment

    # Make sure that we are counting backwards.
    if starting_block_height < ending_block_height:
        tmp_block_height = starting_block_height
        starting_block_height = ending_block_height
        ending_block_height = tmp_block_height

    block_list = []
    # Find Starting Block
    block = blockexplorer.get_block_height(starting_block_height + 2)[0]
    # Yes start incremented by 2 because we decrement here, and before each append.
    block = get_next_valid_block(block)
    while True:
        try:
            while block.height > ending_block_height:
                block = get_next_valid_block(block)
                block_list.append(block)

            outfile = "pickle_data/blocks_{}_to_{}.pkl".format(ending_block_height, starting_block_height)
            with open(outfile, 'wb') as f:
                pickle.dump(block_list, f)
            f.close()
            break
        except Exception as e:
            print(e)
            # If We exceed the number of requests in our time window, wait five minutes and then continue...
            time.sleep(300)


def get_chunks_in_range(start, end, chunksize):
    chunk_start = start
    for i in range(start + chunksize, end + chunksize, chunksize):
        create_block_list(i-1, chunk_start)
        print(chunk_start, i-1)
        chunk_start = i

if __name__ == "__main__":
    # This will generate a series of pickle files, each containing 50 blocks,
    # making it easy to take a break from this program when you need your compute resources.
    # You should also be able to close this laptop, and have this process automatically resume
    # without any problems.

    # if not os.path.isdir("pickle_data"):
    #     os.mkdir("pickle_data")

    get_chunks_in_range(0, 3999, 50)



    my_list = [[1, 2], [2, 3], [3, 4]]
    my_list.sort(key=lambda x: x[-1])
    print(my_list)
