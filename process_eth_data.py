import csv
import os
import pickle
import sys

acct_dir_str = b'./eth_acct_data/'
acct_dir = os.fsencode(acct_dir_str)


t_dir_str = b'./eth_t_data/'
t_dir = os.fsencode(t_dir_str)

# t_chunk_str = ''
t_names = sorted(os.listdir(t_dir), key=lambda x: int(x.decode().split('-')[1]))

acct_dict = pickle.load(open('acct_metadata_all.pkl', 'rb'))
print('account dictionary loaded')
accounts = {}
chunk_size = 10000
acct_chunk_str = ''
node_number = 0
old_t = 0
current_t = 0
for filename in t_names:

    file = t_dir_str + os.fsencode(filename)

    with open( file, 'r' ) as t_file:
        # import pdb; pdb.set_trace()
        t_reader = csv.reader(t_file, delimiter=',')

        for row in t_reader:
            current_t = int(row[3])
            to_id = row[1]
            from_id = row[0]

            if row[2] == '0' or ('none' in (from_id, to_id)) or to_id == from_id:
                continue

            if current_t < old_t:
                print( 'timestamps out of order' )
                sys.exit(0)
            elif current_t != old_t:
                old_t = current_t

            if row[0] not in accounts:
                contract = acct_dict[row[0]][1]
                accounts[row[0]] = [current_t, contract]
                acct_chunk_str += row[0] + '\t'
                acct_chunk_str += '\t'.join([str(current_t), str(contract)]) + '\n'

            if row[1] not in accounts:
                contract = acct_dict[row[1]][1]
                accounts[row[1]] = [current_t, contract]
                acct_chunk_str += row[1] + '\t'
                acct_chunk_str += '\t'.join([str(current_t), str(contract)]) + '\n'

            if (len(accounts) % chunk_size) == 0:

                with open('acct_metadata_all.tsv', 'a') as new_accts:
                    new_accts.write(acct_chunk_str)

                acct_chunk_str = ''

    print(filename.decode() + ' complete, %d accounts' % len(accounts))

with open( 'acct_metadata_all.tsv', 'a') as new_accts:
    new_accts.write( acct_chunk_str )

# pickle.dump( accounts, open('acct_metadata_all.pkl', 'wb'))
# t_dir_str = b'./eth_t_data/'
# t_dir = os.fsencode(t_dir_str)

# t_chunk_str = ''
# t_names = sorted(os.listdir(t_dir), key=lambda x: int(x.decode().split('-')[1]))

# # data to capture all unique transactions within a single timestamp
# # tstamp, address tuples, aggregate values
# t_stamp_data = [0, [],[]] # {(from, to): vals, }

# old_tstamp = 0
# new_tstamp = 0
# count = 0

# for filename in t_names:
#     # files in block order
#     file = t_dir_str + os.fsencode(filename)

#     with open( file, 'r' ) as t_file:

#         new_fname = 'processed' + t_file.name.decode().split('/')[2]
#         t_reader = csv.reader(t_file, delimiter=',')
        
#         for row in t_reader:
#             new_tstamp = row[3]
#             addrs = (row[0], row[1])
#             val = row[2]

#             if val == '0' or ('none' in addrs):
#                 continue

#             count += 1
            
#             if new_tstamp == old_tstamp:
#                 # timestamps are the same add data to timestamp data
#                 if addrs in t_stamp_data:
#                     t_stamp_data[2][t_stamp_data.index(addrs)] += val
#                 else:
#                     t_stamp_data[1].append(addrs)
#                     t_stamp_data[2].append(val)
#             else:
#                 # write timestamp data to string and reset stamp_data
#                 data = [(t_stamp_data[0], tup[0], tup[1], t_stamp_data[2][i]) for i, tup in enumerate(t_stamp_data[1])]
#                 t_chunk_str += ','.join([','.join(tup) + '\n' for tup in data])
#                 old_tstamp = new_tstamp
#                 t_stamp_data = [new_tstamp, [addrs],[val]]

#             if (count % chunk_size) == 0:
#                 with open(new_fname, 'a') as new_ts:
#                     new_ts.write(t_chunk_str)
#                 t_chunk_str = ''

#     with open(new_fname, 'a') as new_ts:
#         new_ts.write(t_chunk_str)






